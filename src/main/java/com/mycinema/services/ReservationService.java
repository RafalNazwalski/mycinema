package com.mycinema.services;

import com.mycinema.domain.nosql.Reservation;
import com.mycinema.domain.nosql.Show;
import com.mycinema.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReservationService {

    @Autowired
    ReservationRepository repo;
    @Autowired
    ShowService showService;

    public List<Reservation> findAll(String userEmail) {
        return repo.findAllByUserEmail(userEmail);
    }

    public List<Reservation> findAll() {
        return repo.findAll();
    }

    public Reservation save(Reservation reservation) {
        return repo.save(reservation);
    }

    public Reservation create(String movieTitle, String hallNumber, String date, String hour, String userEmail, List<String> selectedSeats) {

        Reservation reservation = new Reservation();
        reservation.setMovieTitle(movieTitle);
        reservation.setHallNumber(hallNumber);
        reservation.setDate(date);
        reservation.setHour(hour);
        reservation.setUserEmail(userEmail);
        reservation.setSelectedSeats(selectedSeats);

        Show show = showService.findShow(date, hour, movieTitle);
        for (String s : selectedSeats) {
            String row = s.substring(0, 1);
            String seat = s.substring(2);
            show.reserveSeat(row, seat);
        }
        showService.save(show);

        return repo.save(reservation);
    }

    public void deleteReservation(String reservationId) {
        Reservation reservation = repo.findOne(reservationId);
        Show show = showService.findShow(reservation.getDate(), reservation.getHour(), reservation.getMovieTitle());

        List<String> seats = reservation.getSelectedSeats();

        for (String s : seats) {
            String row = s.substring(0, 1);
            String seat = s.substring(2);
            show.cancelSeatReservation(row, seat);
        }

        showService.save(show);

        repo.delete(reservationId);
    }

}
