package com.mycinema.services;

import com.mycinema.domain.nosql.Movie;

import java.util.List;


public interface IMovieService {

    public List<Movie> getAllMovies();

    public Movie save(Movie movie);

    public void delete(String id);

    public Movie find(String id);

}
