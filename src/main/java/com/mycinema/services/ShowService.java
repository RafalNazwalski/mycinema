package com.mycinema.services;

import com.mycinema.domain.nosql.Show;
import com.mycinema.repository.ShowRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShowService {

    @Autowired
    ShowRepository repo;

    public Show findShow(String date, String hour, String movieTitle) {


        return repo.findOneByDateAndHourAndMovieTitle(date, hour, movieTitle);
    }

    public void save(Show show) {
        repo.save(show);
    }

    public List<Show> findAllShows() {
        return repo.findAll();
    }

}
