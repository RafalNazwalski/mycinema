package com.mycinema.services;

import com.mycinema.domain.EmailExistsException;
import com.mycinema.domain.UserDto;
import com.mycinema.domain.nosql.User;
import com.mycinema.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    public User findByEmail(String username) {
        return userRepository.findByEmail(username);
    }

    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Transactional
    public User registerNewUserAccount(UserDto accountDto)
            throws EmailExistsException {

        if (emailExist(accountDto.getEmail())) {
            throw new EmailExistsException(
                    "There is an account with that email address" + " " + accountDto.getEmail());
        }
        User user = new User();

        user.setPassword(accountDto.getPassword());
        user.setEmail(accountDto.getEmail());

        this.save(user);

        return user;
    }

    private boolean emailExist(String email) {
        User user = userRepository.findByEmail(email);
        if (user != null) {
            return true;
        }
        return false;
    }
}
	

