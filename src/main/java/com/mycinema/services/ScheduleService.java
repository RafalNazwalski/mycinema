package com.mycinema.services;

import com.mycinema.domain.nosql.ScheduleEntry;
import com.mycinema.domain.schedule.DailyPackage;
import com.mycinema.domain.schedule.WeeklyPackage;
import com.mycinema.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


@Service
public class ScheduleService {

    @Autowired
    ScheduleRepository repository;


    public List<WeeklyPackage> getWeeklyPackage(String mTitle) {

        ScheduleEntry entry = repository.findOneByMovieTitle(mTitle);

        LocalDateTime time = LocalDateTime.now();
        Locale userLocale = new Locale("pl");
        List<WeeklyPackage> weeklyList = new ArrayList<WeeklyPackage>();

        for (int i = 0; i < 5; i++) {
            WeeklyPackage weeklyPackage = new WeeklyPackage();
            String month = time.getMonth().getDisplayName(TextStyle.SHORT, userLocale);
            String dayOfMonth = "" + time.getDayOfMonth();
            String dayOfWeek = time.getDayOfWeek().getDisplayName(TextStyle.FULL, userLocale);
            String movieTitle = entry.getMovieTitle();
            String movieDuration = entry.getMovieDuration();
            LocalTime currentTime = time.toLocalTime();

            for (LocalTime t : entry.getPlayTimes()) {
                DailyPackage daily = new DailyPackage();
                daily.setMonth(month);
                daily.setDayOfMonth(dayOfMonth);
                daily.setDayOfWeek(dayOfWeek);
                daily.setMovieTitle(movieTitle);
                daily.setMovieDuration(movieDuration);
                daily.setPlayTime(t);
                if (i == 0) {
                    if (currentTime.isAfter(t.minusMinutes(15))) {
                        daily.setAvailable(false);
                    } else {
                        daily.setAvailable(true);
                    }
                } else {
                    daily.setAvailable(true);
                }

                weeklyPackage.addDailyPackage(daily);
            }
            weeklyList.add(weeklyPackage);
            time = time.plusDays(1);
        }

        return weeklyList;
    }


}
























