package com.mycinema.services;

import com.mycinema.domain.nosql.Movie;
import com.mycinema.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
@Primary
public class MovieServiceImpl implements IMovieService {

    @Autowired
    MovieRepository movieRepository;

    @Override
    public List<Movie> getAllMovies() {
        List<Movie> movies = movieRepository.findAll();

        if (movies == null) {
            movies = new ArrayList<Movie>();
        }
        return movies;
    }

    @Override
    public Movie save(Movie movie) {
        return movieRepository.save(movie);
    }

    @Override
    public void delete(String id) {
        movieRepository.delete(id);
    }

    @Override
    public Movie find(String id) {
        return movieRepository.findOne(id);
    }

}
