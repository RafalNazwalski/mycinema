package com.mycinema.domain;

public enum Genre {
    KOMEDIA, DRAMAT, AKCJA, SCIENCE_FICTION, THRILLER, PSYCHOLOGICZNY, ANIMOWANY, KOMEDIA_ROMANTYCZNA;

    private String name;

    private Genre() {
        name = toName();
    }

    public String getName() {
        return name;
    }

    private String toName() {
        String enumName = super.toString();
        String readableName = "";

        switch (enumName) {
            case "KOMEDIA": {
                readableName = "Komedia";
            }
            break;
            case "DRAMAT": {
                readableName = "Dramat";
            }
            break;
            case "AKCJA": {
                readableName = "Akcja";
            }
            break;
            case "SCIENCE_FICTION": {
                readableName = "Science Fiction";
            }
            break;
            case "THRILLER": {
                readableName = "Thriller";
            }
            break;
            case "PSYCHOLOGICZNY": {
                readableName = "Psychologiczny";
            }
            break;
            case "ANIMOWANY": {
                readableName = "Animowany";
            }
            break;
            case "KOMEDIA_ROMANTYCZNA": {
                readableName = "Komedia romantyczna";
            }
            break;
        }

        return readableName;
    }
}