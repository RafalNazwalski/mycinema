package com.mycinema.domain.schedule;

import java.time.LocalTime;

public class DailyPackage {

    private String dayOfWeek;
    private String month;
    private String dayOfMonth;
    private LocalTime playTime;

    private String movieTitle;
    private String movieDuration;
    private boolean isAvailable;

    public DailyPackage() {

    }


    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(String dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }


    public LocalTime getPlayTime() {
        return playTime;
    }


    public void setPlayTime(LocalTime playTime) {
        this.playTime = playTime;
    }


    public String getMovieTitle() {
        return movieTitle;
    }


    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }


    public String getMovieDuration() {
        return movieDuration;
    }


    public void setMovieDuration(String movieDuration) {
        this.movieDuration = movieDuration;
    }


    public boolean isAvailable() {
        return isAvailable;
    }


    public void setAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }


}
