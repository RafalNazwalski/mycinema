package com.mycinema.domain.schedule;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.List;

@Entity
@Table(name = "Schedule")
public class ScheduleEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String movieTitle;
    private String movieDuration;
    private int hallNumber;
    private boolean isOngoing = true;
    @ElementCollection
    private List<LocalTime> playTimes;

    public ScheduleEntry() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getMovieDuration() {
        return movieDuration;
    }

    public void setMovieDuration(String movieDuration) {
        this.movieDuration = movieDuration;
    }

    public int getHallNumber() {
        return hallNumber;
    }

    public void setHallNumber(int hallNumber) {
        this.hallNumber = hallNumber;
    }

    public boolean isOngoing() {
        return isOngoing;
    }

    public void setOngoing(boolean isOngoing) {
        this.isOngoing = isOngoing;
    }

    public List<LocalTime> getPlayTimes() {
        return playTimes;
    }

    public void setPlayTimes(List<LocalTime> playTimes) {
        this.playTimes = playTimes;
    }


}
