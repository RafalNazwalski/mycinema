package com.mycinema.domain.schedule;

import java.util.ArrayList;
import java.util.List;

public class WeeklyPackage {

    private List<DailyPackage> dailyPackages = new ArrayList<>();

    public WeeklyPackage() {

    }

    public List<DailyPackage> getDailyPackages() {
        return dailyPackages;
    }

    public void setDailyPackages(List<DailyPackage> dailyPackages) {
        this.dailyPackages = dailyPackages;
    }

    public void addDailyPackage(DailyPackage dailyPackage) {
        if (dailyPackages != null) {
            dailyPackages.add(dailyPackage);
        }
    }

}
