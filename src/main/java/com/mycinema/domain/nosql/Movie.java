package com.mycinema.domain.nosql;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "movies")
public class Movie {

    @Id
    private String id;

    private String title;

    private String shortDescription;

    private String description;

    private String premiere;

    private String production;

    private String director;

    private List<String> cast = new ArrayList<String>();

    private List<String> genres = new ArrayList<String>();

    private List<String> imagesUrl = new ArrayList<String>();

    private List<String> trailersUrl = new ArrayList<String>();

    private String playTime;

    public Movie() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPremiere() {
        return premiere;
    }

    public void setPremiere(String premiere) {
        this.premiere = premiere;
    }

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public List<String> getCast() {
        return cast;
    }

    public void setCast(List<String> cast) {
        this.cast = cast;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public void setGenres(String... strings) {
        for (String s : strings) {
            genres.add(s);
        }
    }

    public List<String> getImagesUrl() {
        return imagesUrl;
    }

    public void setImagesUrl(List<String> imagesUrl) {
        this.imagesUrl = imagesUrl;
    }

    public void setImagesUrl(String... strings) {
        for (String s : strings) {
            imagesUrl.add(s);
        }
    }

    public List<String> getTrailersUrl() {
        return trailersUrl;
    }

    public void setTrailersUrl(List<String> trailersUrl) {
        this.trailersUrl = trailersUrl;
    }

    public void setTrailersUrl(String... strings) {
        for (String s : strings) {
            trailersUrl.add(s);
        }
    }

    public String getPlayTime() {
        return playTime;
    }

    public void setPlayTime(String playTime) {
        this.playTime = playTime;
    }


}
