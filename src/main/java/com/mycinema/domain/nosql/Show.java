package com.mycinema.domain.nosql;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document(collection = "shows")
public class Show {

    @Id
    private String id;
    private String movieTitle;
    private String hallNumber;

    private String date;
    private String hour;

    private Seat A_1 = new Seat();
    private Seat A_2 = new Seat();
    private Seat A_3 = new Seat();
    private Seat A_4 = new Seat();
    private Seat A_5 = new Seat();
    private Seat A_6 = new Seat();
    private Seat A_7 = new Seat();
    private Seat A_8 = new Seat();
    private Seat A_9 = new Seat();
    private Seat A_10 = new Seat();
    private Seat B_1 = new Seat();
    private Seat B_2 = new Seat();
    private Seat B_3 = new Seat();
    private Seat B_4 = new Seat();
    private Seat B_5 = new Seat();
    private Seat B_6 = new Seat();
    private Seat B_7 = new Seat();
    private Seat B_8 = new Seat();
    private Seat B_9 = new Seat();
    private Seat B_10 = new Seat();
    private Seat C_1 = new Seat();
    private Seat C_2 = new Seat();
    private Seat C_3 = new Seat();
    private Seat C_4 = new Seat();
    private Seat C_5 = new Seat();
    private Seat C_6 = new Seat();
    private Seat C_7 = new Seat();
    private Seat C_8 = new Seat();
    private Seat C_9 = new Seat();
    private Seat C_10 = new Seat();
    private Seat D_1 = new Seat();
    private Seat D_2 = new Seat();
    private Seat D_3 = new Seat();
    private Seat D_4 = new Seat();
    private Seat D_5 = new Seat();
    private Seat D_6 = new Seat();
    private Seat D_7 = new Seat();
    private Seat D_8 = new Seat();
    private Seat D_9 = new Seat();
    private Seat D_10 = new Seat();
    private Seat E_1 = new Seat();
    private Seat E_2 = new Seat();
    private Seat E_3 = new Seat();
    private Seat E_4 = new Seat();
    private Seat E_5 = new Seat();
    private Seat E_6 = new Seat();
    private Seat E_7 = new Seat();
    private Seat E_8 = new Seat();
    private Seat E_9 = new Seat();
    private Seat E_10 = new Seat();
    private Seat F_1 = new Seat();
    private Seat F_2 = new Seat();
    private Seat F_3 = new Seat();
    private Seat F_4 = new Seat();
    private Seat F_5 = new Seat();
    private Seat F_6 = new Seat();
    private Seat F_7 = new Seat();
    private Seat F_8 = new Seat();
    private Seat F_9 = new Seat();
    private Seat F_10 = new Seat();
    private Seat G_1 = new Seat();
    private Seat G_2 = new Seat();
    private Seat G_3 = new Seat();
    private Seat G_4 = new Seat();
    private Seat G_5 = new Seat();
    private Seat G_6 = new Seat();
    private Seat G_7 = new Seat();
    private Seat G_8 = new Seat();
    private Seat G_9 = new Seat();
    private Seat G_10 = new Seat();
    private Seat H_1 = new Seat();
    private Seat H_2 = new Seat();
    private Seat H_3 = new Seat();
    private Seat H_4 = new Seat();
    private Seat H_5 = new Seat();
    private Seat H_6 = new Seat();
    private Seat H_7 = new Seat();
    private Seat H_8 = new Seat();
    private Seat H_9 = new Seat();
    private Seat H_10 = new Seat();


    public Show() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getHallNumber() {
        return hallNumber;
    }

    public void setHallNumber(String hallNumber) {
        this.hallNumber = hallNumber;
    }


    public Seat getA_1() {
        return A_1;
    }

    public void setA_1(Seat a_1) {
        A_1 = a_1;
    }

    public Seat getA_2() {
        return A_2;
    }

    public void setA_2(Seat a_2) {
        A_2 = a_2;
    }

    public Seat getA_3() {
        return A_3;
    }

    public void setA_3(Seat a_3) {
        A_3 = a_3;
    }

    public Seat getA_4() {
        return A_4;
    }

    public void setA_4(Seat a_4) {
        A_4 = a_4;
    }

    public Seat getA_5() {
        return A_5;
    }

    public void setA_5(Seat a_5) {
        A_5 = a_5;
    }

    public Seat getA_6() {
        return A_6;
    }

    public void setA_6(Seat a_6) {
        A_6 = a_6;
    }

    public Seat getA_7() {
        return A_7;
    }

    public void setA_7(Seat a_7) {
        A_7 = a_7;
    }

    public Seat getA_8() {
        return A_8;
    }

    public void setA_8(Seat a_8) {
        A_8 = a_8;
    }

    public Seat getA_9() {
        return A_9;
    }

    public void setA_9(Seat a_9) {
        A_9 = a_9;
    }

    public Seat getA_10() {
        return A_10;
    }

    public void setA_10(Seat a_10) {
        A_10 = a_10;
    }

    public Seat getB_1() {
        return B_1;
    }

    public void setB_1(Seat b_1) {
        B_1 = b_1;
    }

    public Seat getB_2() {
        return B_2;
    }

    public void setB_2(Seat b_2) {
        B_2 = b_2;
    }

    public Seat getB_3() {
        return B_3;
    }

    public void setB_3(Seat b_3) {
        B_3 = b_3;
    }

    public Seat getB_4() {
        return B_4;
    }

    public void setB_4(Seat b_4) {
        B_4 = b_4;
    }

    public Seat getB_5() {
        return B_5;
    }

    public void setB_5(Seat b_5) {
        B_5 = b_5;
    }

    public Seat getB_6() {
        return B_6;
    }

    public void setB_6(Seat b_6) {
        B_6 = b_6;
    }

    public Seat getB_7() {
        return B_7;
    }

    public void setB_7(Seat b_7) {
        B_7 = b_7;
    }

    public Seat getB_8() {
        return B_8;
    }

    public void setB_8(Seat b_8) {
        B_8 = b_8;
    }

    public Seat getB_9() {
        return B_9;
    }

    public void setB_9(Seat b_9) {
        B_9 = b_9;
    }

    public Seat getB_10() {
        return B_10;
    }

    public void setB_10(Seat b_10) {
        B_10 = b_10;
    }

    public Seat getC_1() {
        return C_1;
    }

    public void setC_1(Seat c_1) {
        C_1 = c_1;
    }

    public Seat getC_2() {
        return C_2;
    }

    public void setC_2(Seat c_2) {
        C_2 = c_2;
    }

    public Seat getC_3() {
        return C_3;
    }

    public void setC_3(Seat c_3) {
        C_3 = c_3;
    }

    public Seat getC_4() {
        return C_4;
    }

    public void setC_4(Seat c_4) {
        C_4 = c_4;
    }

    public Seat getC_5() {
        return C_5;
    }

    public void setC_5(Seat c_5) {
        C_5 = c_5;
    }

    public Seat getC_6() {
        return C_6;
    }

    public void setC_6(Seat c_6) {
        C_6 = c_6;
    }

    public Seat getC_7() {
        return C_7;
    }

    public void setC_7(Seat c_7) {
        C_7 = c_7;
    }

    public Seat getC_8() {
        return C_8;
    }

    public void setC_8(Seat c_8) {
        C_8 = c_8;
    }

    public Seat getC_9() {
        return C_9;
    }

    public void setC_9(Seat c_9) {
        C_9 = c_9;
    }

    public Seat getC_10() {
        return C_10;
    }

    public void setC_10(Seat c_10) {
        C_10 = c_10;
    }

    public Seat getD_1() {
        return D_1;
    }

    public void setD_1(Seat d_1) {
        D_1 = d_1;
    }

    public Seat getD_2() {
        return D_2;
    }

    public void setD_2(Seat d_2) {
        D_2 = d_2;
    }

    public Seat getD_3() {
        return D_3;
    }

    public void setD_3(Seat d_3) {
        D_3 = d_3;
    }

    public Seat getD_4() {
        return D_4;
    }

    public void setD_4(Seat d_4) {
        D_4 = d_4;
    }

    public Seat getD_5() {
        return D_5;
    }

    public void setD_5(Seat d_5) {
        D_5 = d_5;
    }

    public Seat getD_6() {
        return D_6;
    }

    public void setD_6(Seat d_6) {
        D_6 = d_6;
    }

    public Seat getD_7() {
        return D_7;
    }

    public void setD_7(Seat d_7) {
        D_7 = d_7;
    }

    public Seat getD_8() {
        return D_8;
    }

    public void setD_8(Seat d_8) {
        D_8 = d_8;
    }

    public Seat getD_9() {
        return D_9;
    }

    public void setD_9(Seat d_9) {
        D_9 = d_9;
    }

    public Seat getD_10() {
        return D_10;
    }

    public void setD_10(Seat d_10) {
        D_10 = d_10;
    }

    public Seat getE_1() {
        return E_1;
    }

    public void setE_1(Seat e_1) {
        E_1 = e_1;
    }

    public Seat getE_2() {
        return E_2;
    }

    public void setE_2(Seat e_2) {
        E_2 = e_2;
    }

    public Seat getE_3() {
        return E_3;
    }

    public void setE_3(Seat e_3) {
        E_3 = e_3;
    }

    public Seat getE_4() {
        return E_4;
    }

    public void setE_4(Seat e_4) {
        E_4 = e_4;
    }

    public Seat getE_5() {
        return E_5;
    }

    public void setE_5(Seat e_5) {
        E_5 = e_5;
    }

    public Seat getE_6() {
        return E_6;
    }

    public void setE_6(Seat e_6) {
        E_6 = e_6;
    }

    public Seat getE_7() {
        return E_7;
    }

    public void setE_7(Seat e_7) {
        E_7 = e_7;
    }

    public Seat getE_8() {
        return E_8;
    }

    public void setE_8(Seat e_8) {
        E_8 = e_8;
    }

    public Seat getE_9() {
        return E_9;
    }

    public void setE_9(Seat e_9) {
        E_9 = e_9;
    }

    public Seat getE_10() {
        return E_10;
    }

    public void setE_10(Seat e_10) {
        E_10 = e_10;
    }

    public Seat getF_1() {
        return F_1;
    }

    public void setF_1(Seat f_1) {
        F_1 = f_1;
    }

    public Seat getF_2() {
        return F_2;
    }

    public void setF_2(Seat f_2) {
        F_2 = f_2;
    }

    public Seat getF_3() {
        return F_3;
    }

    public void setF_3(Seat f_3) {
        F_3 = f_3;
    }

    public Seat getF_4() {
        return F_4;
    }

    public void setF_4(Seat f_4) {
        F_4 = f_4;
    }

    public Seat getF_5() {
        return F_5;
    }

    public void setF_5(Seat f_5) {
        F_5 = f_5;
    }

    public Seat getF_6() {
        return F_6;
    }

    public void setF_6(Seat f_6) {
        F_6 = f_6;
    }

    public Seat getF_7() {
        return F_7;
    }

    public void setF_7(Seat f_7) {
        F_7 = f_7;
    }

    public Seat getF_8() {
        return F_8;
    }

    public void setF_8(Seat f_8) {
        F_8 = f_8;
    }

    public Seat getF_9() {
        return F_9;
    }

    public void setF_9(Seat f_9) {
        F_9 = f_9;
    }

    public Seat getF_10() {
        return F_10;
    }

    public void setF_10(Seat f_10) {
        F_10 = f_10;
    }

    public Seat getG_1() {
        return G_1;
    }

    public void setG_1(Seat g_1) {
        G_1 = g_1;
    }

    public Seat getG_2() {
        return G_2;
    }

    public void setG_2(Seat g_2) {
        G_2 = g_2;
    }

    public Seat getG_3() {
        return G_3;
    }

    public void setG_3(Seat g_3) {
        G_3 = g_3;
    }

    public Seat getG_4() {
        return G_4;
    }

    public void setG_4(Seat g_4) {
        G_4 = g_4;
    }

    public Seat getG_5() {
        return G_5;
    }

    public void setG_5(Seat g_5) {
        G_5 = g_5;
    }

    public Seat getG_6() {
        return G_6;
    }

    public void setG_6(Seat g_6) {
        G_6 = g_6;
    }

    public Seat getG_7() {
        return G_7;
    }

    public void setG_7(Seat g_7) {
        G_7 = g_7;
    }

    public Seat getG_8() {
        return G_8;
    }

    public void setG_8(Seat g_8) {
        G_8 = g_8;
    }

    public Seat getG_9() {
        return G_9;
    }

    public void setG_9(Seat g_9) {
        G_9 = g_9;
    }

    public Seat getG_10() {
        return G_10;
    }

    public void setG_10(Seat g_10) {
        G_10 = g_10;
    }

    public Seat getH_1() {
        return H_1;
    }

    public void setH_1(Seat h_1) {
        H_1 = h_1;
    }

    public Seat getH_2() {
        return H_2;
    }

    public void setH_2(Seat h_2) {
        H_2 = h_2;
    }

    public Seat getH_3() {
        return H_3;
    }

    public void setH_3(Seat h_3) {
        H_3 = h_3;
    }

    public Seat getH_4() {
        return H_4;
    }

    public void setH_4(Seat h_4) {
        H_4 = h_4;
    }

    public Seat getH_5() {
        return H_5;
    }

    public void setH_5(Seat h_5) {
        H_5 = h_5;
    }

    public Seat getH_6() {
        return H_6;
    }

    public void setH_6(Seat h_6) {
        H_6 = h_6;
    }

    public Seat getH_7() {
        return H_7;
    }

    public void setH_7(Seat h_7) {
        H_7 = h_7;
    }

    public Seat getH_8() {
        return H_8;
    }

    public void setH_8(Seat h_8) {
        H_8 = h_8;
    }

    public Seat getH_9() {
        return H_9;
    }

    public void setH_9(Seat h_9) {
        H_9 = h_9;
    }

    public Seat getH_10() {
        return H_10;
    }

    public void setH_10(Seat h_10) {
        H_10 = h_10;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }


    public boolean isSeatReserved(String row, String seat) {

        boolean isTaken = getSeat(row, seat).isTaken();


        return isTaken;
    }

    public boolean reserveSeat(String row, String seat) {

        boolean isSuccess = false;

        if (getSeat(row, seat).isTaken() == false) {
            getSeat(row, seat).setTaken(true);
            isSuccess = true;
        }


        return isSuccess;
    }

    public boolean cancelSeatReservation(String row, String seat) {

        boolean isSuccess = true;

        getSeat(row, seat).setTaken(false);

        return isSuccess;
    }

    private Seat getSeat(String row, String seat) {
        Seat s = null;

        if (row.equalsIgnoreCase("A")) {
            if (seat.equalsIgnoreCase("1")) {
                s = this.getA_1();
            }
            if (seat.equalsIgnoreCase("2")) {
                s = this.getA_2();
            }
            if (seat.equalsIgnoreCase("3")) {
                s = this.getA_3();
            }
            if (seat.equalsIgnoreCase("4")) {
                s = this.getA_4();
            }
            if (seat.equalsIgnoreCase("5")) {
                s = this.getA_5();
            }
            if (seat.equalsIgnoreCase("6")) {
                s = this.getA_6();
            }
            if (seat.equalsIgnoreCase("7")) {
                s = this.getA_7();
            }
            if (seat.equalsIgnoreCase("8")) {
                s = this.getA_8();
            }
            if (seat.equalsIgnoreCase("9")) {
                s = this.getA_9();
            }
            if (seat.equalsIgnoreCase("10")) {
                s = this.getA_10();
            }
        }

        if (row.equalsIgnoreCase("B")) {
            if (seat.equalsIgnoreCase("1")) {
                s = this.getB_1();
            }
            if (seat.equalsIgnoreCase("2")) {
                s = this.getB_2();
            }
            if (seat.equalsIgnoreCase("3")) {
                s = this.getB_3();
            }
            if (seat.equalsIgnoreCase("4")) {
                s = this.getB_4();
            }
            if (seat.equalsIgnoreCase("5")) {
                s = this.getB_5();
            }
            if (seat.equalsIgnoreCase("6")) {
                s = this.getB_6();
            }
            if (seat.equalsIgnoreCase("7")) {
                s = this.getB_7();
            }
            if (seat.equalsIgnoreCase("8")) {
                s = this.getB_8();
            }
            if (seat.equalsIgnoreCase("9")) {
                s = this.getB_9();
            }
            if (seat.equalsIgnoreCase("10")) {
                s = this.getB_10();
            }
        }

        if (row.equalsIgnoreCase("C")) {
            if (seat.equalsIgnoreCase("1")) {
                s = this.getC_1();
            }
            if (seat.equalsIgnoreCase("2")) {
                s = this.getC_2();
            }
            if (seat.equalsIgnoreCase("3")) {
                s = this.getC_3();
            }
            if (seat.equalsIgnoreCase("4")) {
                s = this.getC_4();
            }
            if (seat.equalsIgnoreCase("5")) {
                s = this.getC_5();
            }
            if (seat.equalsIgnoreCase("6")) {
                s = this.getC_6();
            }
            if (seat.equalsIgnoreCase("7")) {
                s = this.getC_7();
            }
            if (seat.equalsIgnoreCase("8")) {
                s = this.getC_8();
            }
            if (seat.equalsIgnoreCase("9")) {
                s = this.getC_9();
            }
            if (seat.equalsIgnoreCase("10")) {
                s = this.getC_10();
            }
        }

        if (row.equalsIgnoreCase("D")) {
            if (seat.equalsIgnoreCase("1")) {
                s = this.getD_1();
            }
            if (seat.equalsIgnoreCase("2")) {
                s = this.getD_2();
            }
            if (seat.equalsIgnoreCase("3")) {
                s = this.getD_3();
            }
            if (seat.equalsIgnoreCase("4")) {
                s = this.getD_4();
            }
            if (seat.equalsIgnoreCase("5")) {
                s = this.getD_5();
            }
            if (seat.equalsIgnoreCase("6")) {
                s = this.getD_6();
            }
            if (seat.equalsIgnoreCase("7")) {
                s = this.getD_7();
            }
            if (seat.equalsIgnoreCase("8")) {
                s = this.getD_8();
            }
            if (seat.equalsIgnoreCase("9")) {
                s = this.getD_9();
            }
            if (seat.equalsIgnoreCase("10")) {
                s = this.getD_10();
            }
        }

        if (row.equalsIgnoreCase("E")) {
            if (seat.equalsIgnoreCase("1")) {
                s = this.getE_1();
            }
            if (seat.equalsIgnoreCase("2")) {
                s = this.getE_2();
            }
            if (seat.equalsIgnoreCase("3")) {
                s = this.getE_3();
            }
            if (seat.equalsIgnoreCase("4")) {
                s = this.getE_4();
            }
            if (seat.equalsIgnoreCase("5")) {
                s = this.getE_5();
            }
            if (seat.equalsIgnoreCase("6")) {
                s = this.getE_6();
            }
            if (seat.equalsIgnoreCase("7")) {
                s = this.getE_7();
            }
            if (seat.equalsIgnoreCase("8")) {
                s = this.getE_8();
            }
            if (seat.equalsIgnoreCase("9")) {
                s = this.getE_8();
            }
            if (seat.equalsIgnoreCase("10")) {
                s = this.getE_10();
            }
        }

        if (row.equalsIgnoreCase("F")) {
            if (seat.equalsIgnoreCase("1")) {
                s = this.getF_1();
            }
            if (seat.equalsIgnoreCase("2")) {
                s = this.getF_2();
            }
            if (seat.equalsIgnoreCase("3")) {
                s = this.getF_3();
            }
            if (seat.equalsIgnoreCase("4")) {
                s = this.getF_4();
            }
            if (seat.equalsIgnoreCase("5")) {
                s = this.getF_5();
            }
            if (seat.equalsIgnoreCase("6")) {
                s = this.getF_6();
            }
            if (seat.equalsIgnoreCase("7")) {
                s = this.getF_7();
            }
            if (seat.equalsIgnoreCase("8")) {
                s = this.getF_8();
            }
            if (seat.equalsIgnoreCase("9")) {
                s = this.getF_9();
            }
            if (seat.equalsIgnoreCase("10")) {
                s = this.getF_10();
            }
        }

        if (row.equalsIgnoreCase("G")) {
            if (seat.equalsIgnoreCase("1")) {
                s = this.getG_1();
            }
            if (seat.equalsIgnoreCase("2")) {
                s = this.getG_2();
            }
            if (seat.equalsIgnoreCase("3")) {
                s = this.getG_3();
            }
            if (seat.equalsIgnoreCase("4")) {
                s = this.getG_4();
            }
            if (seat.equalsIgnoreCase("5")) {
                s = this.getG_5();
            }
            if (seat.equalsIgnoreCase("6")) {
                s = this.getG_6();
            }
            if (seat.equalsIgnoreCase("7")) {
                s = this.getG_7();
            }
            if (seat.equalsIgnoreCase("8")) {
                s = this.getG_8();
            }
            if (seat.equalsIgnoreCase("9")) {
                s = this.getG_9();
            }
            if (seat.equalsIgnoreCase("10")) {
                s = this.getG_10();
            }
        }

        if (row.equalsIgnoreCase("H")) {
            if (seat.equalsIgnoreCase("1")) {
                s = this.getH_1();
            }
            if (seat.equalsIgnoreCase("2")) {
                s = this.getH_2();
            }
            if (seat.equalsIgnoreCase("3")) {
                s = this.getH_3();
            }
            if (seat.equalsIgnoreCase("4")) {
                s = this.getH_4();
            }
            if (seat.equalsIgnoreCase("5")) {
                s = this.getH_5();
            }
            if (seat.equalsIgnoreCase("6")) {
                s = this.getH_6();
            }
            if (seat.equalsIgnoreCase("7")) {
                s = this.getH_7();
            }
            if (seat.equalsIgnoreCase("8")) {
                s = this.getH_8();
            }
            if (seat.equalsIgnoreCase("9")) {
                s = this.getH_9();
            }
            if (seat.equalsIgnoreCase("10")) {
                s = this.getH_10();
            }
        }


        return s;
    }

}
























