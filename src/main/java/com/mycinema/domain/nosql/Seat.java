package com.mycinema.domain.nosql;

public class Seat implements Furniture {

    private boolean isTaken = false;

    public Seat() {

    }

    public Seat(boolean isTaken) {
        this.isTaken = isTaken;

    }

    public boolean isTaken() {
        return isTaken;
    }

    public void setTaken(boolean isTaken) {
        this.isTaken = isTaken;
    }

}
