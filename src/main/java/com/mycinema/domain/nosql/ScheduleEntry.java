package com.mycinema.domain.nosql;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.time.LocalTime;
import java.util.List;

@Document(collection = "schedules")
public class ScheduleEntry {

    @Id
    private String id;
    private String movieTitle;
    private String movieDuration;
    private int hallNumber;
    private boolean isOngoing = true;
    private List<LocalTime> playTimes;

    public ScheduleEntry() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getMovieDuration() {
        return movieDuration;
    }

    public void setMovieDuration(String movieDuration) {
        this.movieDuration = movieDuration;
    }

    public int getHallNumber() {
        return hallNumber;
    }

    public void setHallNumber(int hallNumber) {
        this.hallNumber = hallNumber;
    }

    public boolean isOngoing() {
        return isOngoing;
    }

    public void setOngoing(boolean isOngoing) {
        this.isOngoing = isOngoing;
    }

    public List<LocalTime> getPlayTimes() {
        return playTimes;
    }

    public void setPlayTimes(List<LocalTime> playTimes) {
        this.playTimes = playTimes;
    }


}
