package com.mycinema.domain;

public class EmailExistsException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public EmailExistsException(String msg) {
        super(msg);
    }

}
