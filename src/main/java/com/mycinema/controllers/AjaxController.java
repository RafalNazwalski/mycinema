package com.mycinema.controllers;

import com.mycinema.domain.ReservationCriteria;
import com.mycinema.domain.SearchCriteria;
import com.mycinema.domain.nosql.Reservation;
import com.mycinema.domain.nosql.Response;
import com.mycinema.domain.nosql.Show;
import com.mycinema.services.ReservationService;
import com.mycinema.services.ShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class AjaxController {

    @Autowired
    ReservationService reservationService;
    @Autowired
    ShowService showService;

    @PostMapping("/api/search")
    public Response getRequestedShow(@RequestBody SearchCriteria search, Errors errors) {
        Show show = showService.findShow(search.getDate(), search.getHour(), search.getMovieTitle());
        return new Response("Done", show);
    }

    @PostMapping("/api/book")
    public Response bookRequestedShow(@RequestBody ReservationCriteria criteria, Errors errors, Principal principal) {

        Show show = showService.findShow(criteria.getDate(), criteria.getHour(), criteria.getMovieTitle());

        Reservation reservation = new Reservation();
        reservation.setMovieTitle(criteria.getMovieTitle());
        reservation.setHallNumber(show.getHallNumber());
        reservation.setDate(criteria.getDate());
        reservation.setHour(criteria.getHour());
        reservation.setSelectedSeats(criteria.getSeats());
        reservation.setUserEmail(principal.getName());

        for (String s : criteria.getSeats()) {
            String row = s.substring(0, 1);
            String seat = s.substring(2);
            show.reserveSeat(row, seat);
        }

        showService.save(show);
        reservationService.save(reservation);

        return new Response("Done", reservation);
    }
}
