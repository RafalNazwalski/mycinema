package com.mycinema.controllers;

import com.mycinema.domain.nosql.Reservation;
import com.mycinema.services.MovieServiceImpl;
import com.mycinema.services.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@Controller
public class RouteController {

    public static final String IS_LOGED = "isLoged";
    public static final String QUEST_GMAIL_COM = "quest@gmail.com";

    @Autowired
    MovieServiceImpl movieService;
    @Autowired
    ReservationService reservationService;

    @GetMapping("/")
    public String getIndex(Principal principal, Model model) {

        if (principal == null) {
            model.addAttribute(IS_LOGED, false);
        } else {
            if (principal.getName().equals(QUEST_GMAIL_COM)) {
                HttpServletRequest request =
                        ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                                .getRequest();
                new SecurityContextLogoutHandler().logout(request, null, null);

                model.addAttribute(IS_LOGED, false);
            } else {
                model.addAttribute(IS_LOGED, true);
            }
        }
        model.addAttribute("movies", movieService.getAllMovies());
        return "index";
    }

    @GetMapping("/detailed/{id}")
    public String getDetailedMovie(@PathVariable String id, Principal principal, Model model) {
        if (principal == null) {
            model.addAttribute(IS_LOGED, false);
        } else {
            if (principal.getName().equals(QUEST_GMAIL_COM)) {
                HttpServletRequest request =
                        ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                                .getRequest();
                new SecurityContextLogoutHandler().logout(request, null, null);
                model.addAttribute(IS_LOGED, false);
            } else {
                model.addAttribute(IS_LOGED, true);
            }
        }
        model.addAttribute("movie", movieService.find(id));

        return "detailed";
    }

    @GetMapping("/user")
    public String getUserReservationPage(Principal principal, Model model) {

        if (principal.getName().equals(QUEST_GMAIL_COM)) {
            return "redirect:/";
        }

        List<Reservation> allReservations = reservationService.findAll(principal.getName());
        boolean canDisplay = true;
        if (allReservations == null) {
            canDisplay = false;
            model.addAttribute("canDisplay", canDisplay);
        }

        model.addAttribute("res", allReservations);
        model.addAttribute(IS_LOGED, true);

        return "user";
    }

    @GetMapping("/user/delete/{id}")
    public String deleteReservation(Principal principal, @PathVariable String id) {

        if (principal.getName().equals(QUEST_GMAIL_COM)) {
            return "redirect:/";
        }
        reservationService.deleteReservation(id);
        return "redirect:/user";
    }

    @GetMapping("/login")
    public String getLoginPage() {
        return "login";
    }
}
































