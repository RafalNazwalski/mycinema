package com.mycinema.controllers;

import com.mycinema.domain.schedule.DailyPackage;
import com.mycinema.domain.schedule.WeeklyPackage;
import com.mycinema.services.ScheduleService;
import com.mycinema.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;


@Controller
public class ReservationController {

    @Autowired
    ScheduleService scheduleService;
    @Autowired
    UserService userService;

    @GetMapping("/reservation/{id}")
    public String getReservationPage(@PathVariable String id, Principal principal, Model model) {

        List<WeeklyPackage> weeklyPackage = scheduleService.getWeeklyPackage(id);
        List<String> days = new ArrayList<>();
        List<List<DailyPackage>> daily = new ArrayList<>();

        for (WeeklyPackage p : weeklyPackage) {
            String day = p.getDailyPackages().get(0).getDayOfWeek();
            String dayNumber = p.getDailyPackages().get(0).getDayOfMonth();
            String month = p.getDailyPackages().get(0).getMonth();

            String outcome = "" + dayNumber + " " + month + ", " + day;
            days.add(outcome);
        }

        for (WeeklyPackage p : weeklyPackage) {
            daily.add(p.getDailyPackages());
        }

        model.addAttribute("days", days);
        model.addAttribute("daily", daily);

        return "reservation";
    }


}
