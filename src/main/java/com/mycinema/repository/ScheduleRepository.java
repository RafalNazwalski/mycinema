package com.mycinema.repository;

import com.mycinema.domain.nosql.ScheduleEntry;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScheduleRepository extends MongoRepository<ScheduleEntry, String> {

    List<ScheduleEntry> findAll();

    ScheduleEntry findOneByMovieTitle(String movieTitle);

}
