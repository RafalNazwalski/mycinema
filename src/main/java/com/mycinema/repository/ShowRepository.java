package com.mycinema.repository;

import com.mycinema.domain.nosql.Show;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShowRepository extends MongoRepository<Show, String> {

    Show findOneByDateAndHourAndMovieTitle(String date, String hour, String movieTitle);

}
