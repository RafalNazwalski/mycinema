package com.mycinema;

import com.mycinema.domain.nosql.Movie;
import com.mycinema.domain.nosql.ScheduleEntry;
import com.mycinema.domain.nosql.Show;
import com.mycinema.domain.nosql.User;
import com.mycinema.repository.MovieRepository;
import com.mycinema.repository.ScheduleRepository;
import com.mycinema.services.ShowService;
import com.mycinema.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


@Component
public class AppStartupRunner implements CommandLineRunner {

    @Autowired
    MovieRepository movieRepository;
    @Autowired
    ScheduleRepository scheduleRepository;
    @Autowired
    UserService userService;
    @Autowired
    ShowService showService;

    @Override
    public void run(String... arg0) {
        createMoviesEntry();
        createScheduleEntry();
        addUser();
        createShows();
    }

    private void createMoviesEntry() {

        Movie mov1 = new Movie();
        mov1.setTitle("Avengers: Infinity War (2016)");
        mov1.setShortDescription("Potężny Thanos zbiera Kamienie Nieskończoności w celu narzucenia swojej woli wszystkim istnieniom we wszechświecie. " +
                "Tylko drużyna superbohaterów znanych jako Avengers może go powstrzymać.");
        mov1.setDescription("Avengers i ich sojusznicy chronili świat przed zagrożeniem tak dużym, że jeden superbohater nie był w stanie sobie z tym poradzić. " +
                "Z kosmosu nadchodzi nowe zagrożenie: Thanos. Okryty niesławą intergalaktyczny despota," +
                "którego celem jest zebranie sześciu Kamieni Nieskończoności. " +
                "Chce wykorzystać te artefakty o niewyobrażalnej mocy, aby narzucić wszystkim swoją wolę. " +
                "Wszystko, o co do tej pory Avengers walczyli, prowadziło do tej chwili. Los Ziemi i wszelkiego istnienia nigdy nie był tak bardzo niepewny.");
        mov1.setDirector("Anthony Russo Joe Russo");
        mov1.setProduction("USA");
        mov1.setGenres("Akcja", "Sci-Fi");
        mov1.setPlayTime("2 godz. 31 min.");
        mov1.setImagesUrl("/images/The-Avengers-Infinity-War-center-top-700x400.jpg");
        mov1.setTrailersUrl("6ZfuNTqbHE8", "pVxOVlm_lE8", "3VbHg5fqBYw", "sAOzrChqmd0");

        Movie mov2 = new Movie();
        mov2.setTitle("Batman vs Superman: Dawn of Justice (2016)");
        mov2.setShortDescription("W obliczu wielkiego zagrożenia Batman niechętnie łączy siły z Supermanem. Poza herosami do walki staje także wojownicza Wonder Woman.");
        mov2.setDescription("W obawie przed poczynaniami nieposkromionego superbohatera o boskich rysach i zdolnościach, " +
                "najznamienitszy obywatel Gotham City, a zarazem zaciekły strażnik porządku, " +
                "staje do walki z otaczanym czcią współczesnym wybawcą Metropolis, " +
                "podczas gdy świat usiłuje ustalić, jakiego bohatera naprawdę potrzebuje. Wobec konfliktu, który rozgorzał między Batmanem i Supermanem, " +
                "na horyzoncie szybko pojawia się nowy wróg, " +
                "stawiając ludzkość w obliczu śmiertelnego niebezpieczeństwa, z jakim jeszcze nigdy nie musiała się mierzyć.");
        mov2.setDirector("Zack Snyder");
        mov2.setProduction("USA");
        mov2.setGenres("Akcja", "Sci-Fi");
        mov2.setPlayTime("2 godz. 31 min.");
        mov2.setImagesUrl("/images/472622-batmanvssuperman-poster.jpg");
        mov2.setTrailersUrl("0WWzgGyAH6Y", "IwfUnkBfdZ4", "fis-9Zqu2Ro", "NhWg7AQLI_8");

        Movie mov3 = new Movie();
        mov3.setTitle("Justice League (2018)");
        mov3.setShortDescription("Jedni z największych superbohaterów na świecie tworzą Ligę Sprawiedliwości. Jej celem jest zwalczanie zagrożeń, które wykraczają poza ludzkie możliwości.");
        mov3.setDescription("Zainspirowany ofiarnym czynem Supermana Bruce Wayne, który odzyskał wiarę w ludzkość, prosi swojego nowego sprzymierzeńca, " +
                "Dianę Prince, o pomoc w pokonaniu jeszcze potężniejszego wroga. Nie tracąc czasu, Batman i Wonder Woman starają się znaleźć i przekonać do współpracy grupę metaludzi, " +
                "która ma stanąć do walki z nowym zagrożeniem. Jednak mimo utworzenia jedynej w swoim rodzaju ligi superbohaterów, złożonej z Batmana, Wonder Woman," +
                " Aquamana, Cyborga i Flasha, może być już za późno na uratowanie planety przed atakiem katastroficznych rozmiarów.");
        mov3.setDirector("Zack Snyder");
        mov3.setProduction("USA");
        mov3.setGenres("Akcja", "Sci-Fi");
        mov3.setPlayTime("2 godz.");
        mov3.setImagesUrl("/images/community-center-6.jpg");
        mov3.setTrailersUrl("r9-DM9uBtVI", "3cxixDgHUYw", "pbnSaCTtZ2Q", "g_6yBZKj-eo");

        Movie mov4 = new Movie();
        mov4.setTitle("Chappie (2015)");
        mov4.setShortDescription("Troje gangsterów kradnie policyjnego androida, by wykorzystać go do swoich celów.");
        mov4.setDescription("W dzisiejszych czasach roboty do zadań specjalnych stają się codziennością. W brutalnym świecie Johannesburga policja i wojsko używają ich do tłumienia zamieszek i walki z gangsterami. " +
                "Chappie jest jednym z robotów nowej generacji, które myślą i czują. Niewiele różni się od nas, ludzi. Jest jak dziecko i tak jak ono poznaje świat oczyma opiekunów. " +
                "Kiedy Chappie dostaje się w ręce gangsterów, ci resetują jego dotychczasową wiedzę i uczą wszystkiego od nowa, " +
                "tak by pracował dla nich. Poczciwy, budzący sympatię robot, nie do końca świadomy wyrządzania zła, uczestniczy w brutalnych napadach rabunkowych i życiu przestępczego półświatka.");
        mov4.setDirector("Neill Blomkamp");
        mov4.setProduction("RPA USA");
        mov4.setGenres("Akcja", "Sci-Fi");
        mov4.setPlayTime("2 godz.");
        mov4.setImagesUrl("/images/chappie_8321.jpg");
        mov4.setTrailersUrl("lyy7y0QOK-0", "=pG8mCC1ZQ1g", "HhNshgSYF_M", "S56sznW3EBk");

        Movie mov5 = new Movie();
        mov5.setTitle("Ex Machina (2015)");
        mov5.setShortDescription("Po wygraniu konkursu programista jednej z największych firm internetowych zostaje zaproszony do posiadłości swojego szefa. Na miejscu dowiaduje się, że jest częścią eksperymentu.");
        mov5.setDescription("Scenarzysta „28 dni później” i „W stronę słońca” zaprasza do świata przyszłości, która dzieje się już teraz, w laboratoriach, o których nie mamy pojęcia. " +
                " Młody informatyk Caleb (Dohmnall Gleeson) wygrywa firmowy konkurs i na tydzień trafia do laboratorium, w którym pracuje szef firmy - Nathan (Oscar Isaac). " +
                " Caleb poznaje w nim piękną Avę (Alicia Vikander) - pierwszą na świecie sztuczną inteligencję pod postacią kobiety. Ava jest wynikiem eksperymentu Nathana. " +
                " Czuje, myśli, wzrusza się i boi o własną przyszłość. Caleb nie wie, że sam jest częścią eksperymentu, który ma sprawdzić, jak na postać Avy zareaguje przeciętny człowiek.");
        mov5.setDirector("Alex Garland");
        mov5.setProduction("Wielka Brytania");
        mov5.setGenres("Thriller", "Sci-Fi");
        mov5.setPlayTime("1 godz. 48 min.");
        mov5.setImagesUrl("/images/EX-Machina-Movie-2015-1100x688-700x400.jpg");
        mov5.setTrailersUrl("sNExF5WYMaA", "zYjDApK6_oE", "ZQjWGBP3Cy0", "EoQuVnKhxaM");

        Movie mov6 = new Movie();
        mov6.setTitle("Nietykalni (2011)");
        mov6.setShortDescription("Sparaliżowany milioner zatrudnia do opieki młodego chłopaka z przedmieścia, który właśnie wyszedł z więzienia.");
        mov6.setDescription("Ta historia zdarzyła się naprawdę. Sparaliżowany na skutek wypadku milioner zatrudnia do pomocy i opieki młodego chłopaka z przedmieścia, który właśnie wyszedł z więzienia. " +
                "Zderzenie dwóch skrajnie różnych światów daje początek szeregowi niewiarygodnych przygód i przyjaźni, która czyni ich... nietykalnymi.");
        mov6.setDirector("Olivier Nakache Eric Toledano");
        mov6.setProduction("Francja");
        mov6.setGenres("Biograficzny", "Dramat", "Komedia");
        mov6.setPlayTime("1 godz. 52 min.");
        mov6.setImagesUrl("/images/maxresdefault-98.jpg");
        mov6.setTrailersUrl("34WIbmXkewU", "0RqDiYnFxTk", "VdAfINhjkXI", "LJx1csOALac");

        Movie mov7 = new Movie();
        mov7.setTitle("Ojciec chrzestny (1972)");
        mov7.setShortDescription("Opowieść o nowojorskiej rodzinie mafijnej. Starzejący się Don Corleone pragnie przekazać władzę swojemu synowi.");
        mov7.setDescription("Vito Corleone to człowiek, który w ciągu kilku dekad z biednego sycylijskiego imigranta stał się „ojcem chrzestnym” jednej z najpotężniejszych mafijnych rodzin w USA. " +
                "Ma on oddaną żonę, córkę Connie oraz trzech synów:  Santina, Frederica, Michaela, spośród których najstarszy (Santino) szykowany jest na jego następcę. " +
                "Kiedy jednak Don Corleone zostaje postrzelony na rozkaz innej rodziny, niezadowolonej z tego, " +
                "że odrzucił ich propozycję współpracy (wejścia w nowy narkotykowy biznes), wybucha „wojna” w której Santino niezbyt sobie radzi jako nowy Don. " +
                "Z pomocą bratu, a przede wszystkim, by chronić rannego ojca, przybywa więc Michael, najmłodszy z braci, świeży weteran i bohater II wojny światowej, " +
                "który wcześniej nie chciał mieć nic wspólnego z rodzinnym interesem. Dalsza część filmu, jak się łatwo domyślić, pokazuje drogę tego młodego, " +
                "ambitnego człowieka na szczyty władzy i jego przemianę w bezwzględnego Dona rodziny Corleone.");
        mov7.setDirector("Francis Ford Coppola");
        mov7.setProduction("USA");
        mov7.setGenres("Dramat", "Gangsterski");
        mov7.setPlayTime("2 godz. 55 min.");
        mov7.setImagesUrl("/images/the-godfather-1.jpg");
        mov7.setTrailersUrl("5DO-nDW43Ik", "fB_8VCwXydM", "VBl_gvTBO9g", "Tk3WSsRoHgQ");

        Movie mov8 = new Movie();
        mov8.setTitle("Infiltracja (2006)");
        mov8.setShortDescription("Tajny policjant w szeregach grupy przestępczej i informator mafii w bostońskiej jednostce dochodzeniowej toczą ze sobą śmiertelną rozgrywkę.");
        mov8.setDescription("Południowy Boston. Policja stanowa pracuje nad ściśle tajnym projektem, którego założeniem jest walka z przestępczością zorganizowaną. " +
                "Doskonale przeszkolony Billy Costigan ma wniknąć w szeregi mafii i zyskać zaufanie jej bossa - Franka Costella. " +
                "W tym samym czasie pracę w jednostce dochodzeniowej w Massachusetts rozpoczyna bystry i niezwykle ambitny Colin Sullivan. " +
                "Młody mężczyzna szybko wspina się po szczeblach kariery i staje się członkiem elitarnego grona kilku inspektorów, " +
                "mających dostęp do ściśle tajnych informacji. Nikt nie wie, że pracuje dla Costella, który niegdyś wziął go pod swoje skrzydła i wykształcił. " +
                "Dzięki niemu irlandzki gangster zawsze zna z wyprzedzeniem plany stróżów prawa i pozostaje nieuchwytny. " +
                "Zarówno Colin, jak i Billy są pochłonięci prowadzeniem podwójnego życia. Nie tracą czujności, " +
                "gdyż nawet najmniejszy błąd może ich zdemaskować. Gdy staje się jasne, że w szeregach policji i mafii działają szpiedzy, " +
                "funkcjonariusze rozpoczynają wyścig z czasem o to, kto pierwszy odkryje tożsamość przeciwnika. Pojedynek jest zacięty, gdyż obaj są mistrzami w swoim fachu.");
        mov8.setDirector("Martin Scorsese");
        mov8.setProduction("Hongkong, USA");
        mov8.setGenres("Kryminał");
        mov8.setPlayTime("2 godz. 32 min.");
        mov8.setImagesUrl("/images/msdep.jpg");
        mov8.setTrailersUrl("n4O3x5BH18E", "b66y1LHixqY", "V3f9UJTmgd0", "yXQEq9nCTD4");

        movieRepository.save(mov1);
        movieRepository.save(mov2);
        movieRepository.save(mov3);
        movieRepository.save(mov4);
        movieRepository.save(mov5);
        movieRepository.save(mov6);
        movieRepository.save(mov7);
        movieRepository.save(mov8);
    }

    private void createScheduleEntry() {

        LocalTime time1 = LocalTime.of(9, 0);
        LocalTime time2 = LocalTime.of(12, 0);
        LocalTime time3 = LocalTime.of(15, 0);
        LocalTime time4 = LocalTime.of(18, 0);
        LocalTime time5 = LocalTime.of(21, 0);

        List<LocalTime> times = new ArrayList<>();
        times.add(time1);
        times.add(time2);
        times.add(time3);
        times.add(time4);
        times.add(time5);

        for (Movie m : movieRepository.findAll()) {
            ScheduleEntry entry1 = new ScheduleEntry();
            entry1.setHallNumber(1);
            entry1.setMovieTitle(m.getTitle());
            entry1.setMovieDuration(m.getPlayTime());
            entry1.setPlayTimes(times);
            scheduleRepository.save(entry1);
        }
    }

    private void addUser() {
        User user = new User();
        user.setEmail("quest@gmail.com");
        user.setPassword("quest");

        User user2 = new User();
        user2.setEmail("rafalnazwalski@gmail.com");
        user2.setPassword("sukces1337");

        userService.save(user);
        userService.save(user2);
    }

    private void createShows() {

        Locale userLocale = new Locale("pl");
        for (ScheduleEntry e : scheduleRepository.findAll()) {
            LocalDateTime time = LocalDateTime.now();
            for (int i = 0; i < 10; i++) {
                for (LocalTime t : e.getPlayTimes()) {
                    String dayOfMonth = "" + time.getDayOfMonth();
                    String dayOfWeek = time.getDayOfWeek().getDisplayName(TextStyle.FULL, userLocale);
                    String month = time.getMonth().getDisplayName(TextStyle.SHORT, userLocale);
                    String combined = dayOfMonth + " " + month + ", " + dayOfWeek;
                    String hour = t.format(DateTimeFormatter.ofPattern("HH:mm"));
                    Show show = new Show();
                    show.setMovieTitle(e.getMovieTitle());
                    show.setHallNumber("" + e.getHallNumber());
                    show.setDate(combined);
                    show.setHour(hour);
                    showService.save(show);
                }
                time = time.plusDays(1L);
            }
        }
    }

}






























































































