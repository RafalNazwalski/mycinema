var date = "";
var hour = "";
var movieTitle = "";
var seats = [];

$(document).ready(function () {

    // Toolbar extra buttons
    var btnFinish = $('<button></button>').text('Koniec')
        .addClass('btn btn-info')
        .on('click', function () {
            $(location).attr('href', '/');
        });
    var btnCancel = $('<button></button>').text('Anuluj')
        .addClass('btn btn-danger')
        .on('click', function () {
            $(location).attr('href', '/');
        });

    // Smart Wizard
    $('#smartwizard').smartWizard({
        selected: 0,
        theme: 'arrows',
        transitionEffect: 'fade',
        backButtonSupport: false, // Enable the back button support
        showStepURLhash: false,
        lang: {  // Language variables
            next: 'Dalej',
            previous: 'Wróc'
        },
        toolbarSettings: {
            toolbarPosition: 'bottom',
            toolbarExtraButtons: [btnCancel, btnFinish],
            showPreviousButton: false,
            showNextButton: true
        },
        anchorSettings: {
            anchorClickable: false, // Enable/Disable anchor navigation
            enableAllAnchors: false, // Activates all anchors clickable all times
            markDoneStep: true, // add done css
            enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
        }
    });

    //$('#btnCancel).hide(); /* possible values: disable, enable, show, hide */

    $('.btn-info').hide();

    $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection) {

        if (stepNumber == 1) {
            $('.sw-btn-next').attr('disabled', true);
        }

        if (stepNumber == 2) {
            $('.sw-btn-next').attr('disabled', true);
            movieTitle = $('#sp31').text();

            var search = {};
            search["date"] = date;
            search["hour"] = hour;
            search["movieTitle"] = movieTitle;

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/api/search",
                data: JSON.stringify(search),
                dataType: 'json',
                cache: false,
                timeout: 600000,
                success: function (result) {

                    var tmp = result.status;
                    if (result.data.a_1.taken == true) {
                        $('#A_1').addClass('seatUnavailable');
                    }
                    if (result.data.a_2.taken == true) {
                        $('#A_2').addClass('seatUnavailable');
                    }
                    if (result.data.a_3.taken == true) {
                        $('#A_3').addClass('seatUnavailable');
                    }
                    if (result.data.a_4.taken == true) {
                        $('#A_4').addClass('seatUnavailable');
                    }
                    if (result.data.a_5.taken == true) {
                        $('#A_5').addClass('seatUnavailable');
                    }
                    if (result.data.a_6.taken == true) {
                        $('#A_6').addClass('seatUnavailable');
                    }
                    if (result.data.a_7.taken == true) {
                        $('#A_7').addClass('seatUnavailable');
                    }
                    if (result.data.a_8.taken == true) {
                        $('#A_8').addClass('seatUnavailable');
                    }
                    if (result.data.a_9.taken == true) {
                        $('#A_9').addClass('seatUnavailable');
                    }
                    if (result.data.a_10.taken == true) {
                        $('#A_10').addClass('seatUnavailable');
                    }
                    if (result.data.b_1.taken == true) {
                        $('#B_1').addClass('seatUnavailable');
                    }
                    if (result.data.b_2.taken == true) {
                        $('#B_2').addClass('seatUnavailable');
                    }
                    if (result.data.b_3.taken == true) {
                        $('#B_3').addClass('seatUnavailable');
                    }
                    if (result.data.b_4.taken == true) {
                        $('#B_4').addClass('seatUnavailable');
                    }
                    if (result.data.b_5.taken == true) {
                        $('#B_5').addClass('seatUnavailable');
                    }
                    if (result.data.b_6.taken == true) {
                        $('#B_6').addClass('seatUnavailable');
                    }
                    if (result.data.b_7.taken == true) {
                        $('#B_7').addClass('seatUnavailable');
                    }
                    if (result.data.b_8.taken == true) {
                        $('#B_8').addClass('seatUnavailable');
                    }
                    if (result.data.b_9.taken == true) {
                        $('#B_9').addClass('seatUnavailable');
                    }
                    if (result.data.b_10.taken == true) {
                        $('#B_10').addClass('seatUnavailable');
                    }
                    if (result.data.c_1.taken == true) {
                        $('#C_1').addClass('seatUnavailable');
                    }
                    if (result.data.c_2.taken == true) {
                        $('#C_2').addClass('seatUnavailable');
                    }
                    if (result.data.c_3.taken == true) {
                        $('#C_3').addClass('seatUnavailable');
                    }
                    if (result.data.c_4.taken == true) {
                        $('#C_4').addClass('seatUnavailable');
                    }
                    if (result.data.c_5.taken == true) {
                        $('#C_5').addClass('seatUnavailable');
                    }
                    if (result.data.c_6.taken == true) {
                        $('#C_6').addClass('seatUnavailable');
                    }
                    if (result.data.c_7.taken == true) {
                        $('#C_7').addClass('seatUnavailable');
                    }
                    if (result.data.c_8.taken == true) {
                        $('#C_8').addClass('seatUnavailable');
                    }
                    if (result.data.c_9.taken == true) {
                        $('#C_9').addClass('seatUnavailable');
                    }
                    if (result.data.c_10.taken == true) {
                        $('#C_10').addClass('seatUnavailable');
                    }
                    if (result.data.d_1.taken == true) {
                        $('#D_1').addClass('seatUnavailable');
                    }
                    if (result.data.d_2.taken == true) {
                        $('#D_2').addClass('seatUnavailable');
                    }
                    if (result.data.d_3.taken == true) {
                        $('#D_3').addClass('seatUnavailable');
                    }
                    if (result.data.d_4.taken == true) {
                        $('#D_4').addClass('seatUnavailable');
                    }
                    if (result.data.d_5.taken == true) {
                        $('#D_5').addClass('seatUnavailable');
                    }
                    if (result.data.d_6.taken == true) {
                        $('#D_6').addClass('seatUnavailable');
                    }
                    if (result.data.d_7.taken == true) {
                        $('#D_7').addClass('seatUnavailable');
                    }
                    if (result.data.d_8.taken == true) {
                        $('#D_8').addClass('seatUnavailable');
                    }
                    if (result.data.d_9.taken == true) {
                        $('#D_9').addClass('seatUnavailable');
                    }
                    if (result.data.d_10.taken == true) {
                        $('#D_10').addClass('seatUnavailable');
                    }
                    if (result.data.e_1.taken == true) {
                        $('#E_1').addClass('seatUnavailable');
                    }
                    if (result.data.e_2.taken == true) {
                        $('#E_2').addClass('seatUnavailable');
                    }
                    if (result.data.e_3.taken == true) {
                        $('#E_3').addClass('seatUnavailable');
                    }
                    if (result.data.e_4.taken == true) {
                        $('#E_4').addClass('seatUnavailable');
                    }
                    if (result.data.e_5.taken == true) {
                        $('#E_5').addClass('seatUnavailable');
                    }
                    if (result.data.e_6.taken == true) {
                        $('#E_6').addClass('seatUnavailable');
                    }
                    if (result.data.e_7.taken == true) {
                        $('#E_7').addClass('seatUnavailable');
                    }
                    if (result.data.e_8.taken == true) {
                        $('#E_8').addClass('seatUnavailable');
                    }
                    if (result.data.e_9.taken == true) {
                        $('#E_9').addClass('seatUnavailable');
                    }
                    if (result.data.e_10.taken == true) {
                        $('#E_10').addClass('seatUnavailable');
                    }
                    if (result.data.f_1.taken == true) {
                        $('#F_1').addClass('seatUnavailable');
                    }
                    if (result.data.f_3.taken == true) {
                        $('#F_2').addClass('seatUnavailable');
                    }
                    if (result.data.f_3.taken == true) {
                        $('#F_3').addClass('seatUnavailable');
                    }
                    if (result.data.f_4.taken == true) {
                        $('#F_4').addClass('seatUnavailable');
                    }
                    if (result.data.f_5.taken == true) {
                        $('#F_5').addClass('seatUnavailable');
                    }
                    if (result.data.f_6.taken == true) {
                        $('#F_6').addClass('seatUnavailable');
                    }
                    if (result.data.f_7.taken == true) {
                        $('#F_7').addClass('seatUnavailable');
                    }
                    if (result.data.f_8.taken == true) {
                        $('#F_8').addClass('seatUnavailable');
                    }
                    if (result.data.f_9.taken == true) {
                        $('#F_9').addClass('seatUnavailable');
                    }
                    if (result.data.f_10.taken == true) {
                        $('#F_10').addClass('seatUnavailable');
                    }
                    if (result.data.g_1.taken == true) {
                        $('#G_1').addClass('seatUnavailable');
                    }
                    if (result.data.g_2.taken == true) {
                        $('#G_2').addClass('seatUnavailable');
                    }
                    if (result.data.g_3.taken == true) {
                        $('#G_3').addClass('seatUnavailable');
                    }
                    if (result.data.g_4.taken == true) {
                        $('#G_4').addClass('seatUnavailable');
                    }
                    if (result.data.g_5.taken == true) {
                        $('#G_5').addClass('seatUnavailable');
                    }
                    if (result.data.g_6.taken == true) {
                        $('#G_6').addClass('seatUnavailable');
                    }
                    if (result.data.g_7.taken == true) {
                        $('#G_7').addClass('seatUnavailable');
                    }
                    if (result.data.g_8.taken == true) {
                        $('#G_8').addClass('seatUnavailable');
                    }
                    if (result.data.g_9.taken == true) {
                        $('#G_9').addClass('seatUnavailable');
                    }
                    if (result.data.g_10.taken == true) {
                        $('#G_10').addClass('seatUnavailable');
                    }
                    if (result.data.h_1.taken == true) {
                        $('#H_1').addClass('seatUnavailable');
                    }
                    if (result.data.h_2.taken == true) {
                        $('#H_2').addClass('seatUnavailable');
                    }
                    if (result.data.h_3.taken == true) {
                        $('#H_3').addClass('seatUnavailable');
                    }
                    if (result.data.h_4.taken == true) {
                        $('#H_4').addClass('seatUnavailable');
                    }
                    if (result.data.h_5.taken == true) {
                        $('#H_5').addClass('seatUnavailable');
                    }
                    if (result.data.h_6.taken == true) {
                        $('#H_6').addClass('seatUnavailable');
                    }
                    if (result.data.h_7.taken == true) {
                        $('#H_7').addClass('seatUnavailable');
                    }
                    if (result.data.h_8.taken == true) {
                        $('#H_8').addClass('seatUnavailable');
                    }
                    if (result.data.h_9.taken == true) {
                        $('#H_9').addClass('seatUnavailable');
                    }
                    if (result.data.h_10.taken == true) {
                        $('#H_10').addClass('seatUnavailable');
                    }


                },
                error: function (e) {

                    alert("Error");

                }
            });
        }

        if (stepNumber == 3) {
            $('.btn-info').show();
            $('.btn-danger').hide();

            movieTitle = $('#sp31').text();

            var search = {}
            search["date"] = date;
            search["hour"] = hour;
            search["movieTitle"] = movieTitle;
            search["seats"] = seats;

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/api/book",
                data: JSON.stringify(search),
                dataType: 'json',
                cache: false,
                timeout: 600000,
                success: function (result) {

                    var tmp = result.status;


                    $("#sp32").text(result.data.userEmail);
                    $("#sp33").text(result.data.date);
                    $("#sp34").text(result.data.hour);
                    $("#sp35").text(result.data.selectedSeats);
                    $("#sp36").text(result.data.id);
                },
                error: function (e) {

                    alert("Error");

                }
            });
        } else {
            $('.btn-info').hide();
            $('.btn-danger').show();
        }

    });

});

$(document).ready(function () {
    $(document).ready(function () {
        $('.sw-btn-next').attr('disabled', true);
        $('.dial1').hide();
        $('.dial2').hide();
        $('.dial3').hide();
        $('.dial4').hide();
        $('.dial5').hide();
        $('input[type=radio][name=options]').change(function () {
            if (this.id == 'option1') {
                $('.sw-btn-next').attr('disabled', false);
                $('.dial1').show();
                $('.dial2').hide();
                $('.dial3').hide();
                $('.dial4').hide();
                $('.dial5').hide();
                date = $('#sp1').text();
                ;
            }
            else if (this.id == 'option2') {
                $('.sw-btn-next').attr('disabled', false);
                $('.dial2').show();
                $('.dial1').hide();
                $('.dial3').hide();
                $('.dial4').hide();
                $('.dial5').hide();
                date = $('#sp2').text();
                ;
            }
            else if (this.id == 'option3') {
                $('.sw-btn-next').attr('disabled', false);
                $('.dial3').show();
                $('.dial1').hide();
                $('.dial2').hide();
                $('.dial4').hide();
                $('.dial5').hide();
                date = $('#sp3').text();
                ;
            }
            else if (this.id == 'option4') {
                $('.sw-btn-next').attr('disabled', false);
                $('.dial4').show();
                $('.dial1').hide();
                $('.dial2').hide();
                $('.dial3').hide();
                $('.dial5').hide();
                date = $('#sp4').text();
                ;
            }
            else if (this.id == 'option5') {
                $('.sw-btn-next').attr('disabled', false);
                $('.dial5').show();
                $('.dial1').hide();
                $('.dial2').hide();
                $('.dial3').hide();
                $('.dial4').hide();
                date = $('#sp5').text();
                ;

            } else if (this.id == 'option6') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp6').text();
                ;

            } else if (this.id == 'option7') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp7').text();
                ;
            } else if (this.id == 'option8') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp8').text();
                ;
            } else if (this.id == 'option9') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp9').text();
                ;
            } else if (this.id == 'option10') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp10').text();
                ;
            } else if (this.id == 'option11') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp11').text();
                ;
            } else if (this.id == 'option12') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp12').text();
                ;
            } else if (this.id == 'option13') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp13').text();
                ;
            } else if (this.id == 'option14') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp14').text();
                ;
            } else if (this.id == 'option15') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp15').text();
                ;
            } else if (this.id == 'option16') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp16').text();
                ;
            } else if (this.id == 'option17') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp17').text();
                ;
            } else if (this.id == 'option18') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp18').text();
                ;
            } else if (this.id == 'option19') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp19').text();
                ;
            } else if (this.id == 'option20') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp20').text();
                ;
            } else if (this.id == 'option21') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp21').text();
                ;
            } else if (this.id == 'option22') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp22').text();
                ;
            } else if (this.id == 'option23') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp23').text();
                ;
            } else if (this.id == 'option24') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp24').text();
                ;
            } else if (this.id == 'option25') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp25').text();
                ;
            } else if (this.id == 'option26') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp26').text();
                ;
            } else if (this.id == 'option27') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp27').text();
                ;
            } else if (this.id == 'option28') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp28').text();
                ;
            } else if (this.id == 'option29') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp29').text();
                ;
            } else if (this.id == 'option30') {
                $('.sw-btn-next').attr('disabled', false);
                hour = $('#sp30').text();
                ;
            }
        });
    });
});

$(document).ready(function () {
    // Clicking any seat
    $(".seatNumber").click(
        function () {
            if (!$(this).hasClass("seatUnavailable")) {
                // If selected, unselect it
                if ($(this).hasClass("seatSelected")) {
                    var thisId = $(this).attr('id');
                    var price = $('#seatsList .' + thisId).val();
                    $(this).removeClass("seatSelected");
                    $('#seatsList .' + thisId).remove();
                    // Calling functions to update checkout total and seat counter.
                    var t = parseInt(seats.indexOf(thisId));
                    if (t == -1) {

                    } else {

                        seats.splice(t, 1);
                    }
                    removeFromCheckout(price);
                    refreshCounter();
                }
                else {
                    // else, select it
                    // getting values from Seat
                    var thisId = $(this).attr('id');
                    var id = thisId.split("_");
                    var price = $(this).attr('value');
                    var seatDetails = "Rząd: " + id[0] + " Miejsce:" + id[1] + " Cena:PLN:" + price;
                    seats.push(thisId);

                    var freeSeats = 0;
                    var selectedSeats = parseInt($(".seatSelected").length);

                    // If you have free seats available the price of this one will be 0.
                    if (selectedSeats < freeSeats) {
                        price = 0;
                    }

                    // Adding this seat to the list
                    var seatDetails = "Rząd: " + id[0] + " Miejsce:" + id[1] + " Cena:PLN:" + price;
                    $("#seatsList").append('<li value=' + price + ' class=' + thisId + '>' + seatDetails + "  " + "<button id='remove:" + thisId + "'+ class='btn btn-default btn-sm removeSeat' value='" + price + "'><strong>X</strong></button></li>");
                    $(this).addClass("seatSelected");

                    addToCheckout(price);
                    refreshCounter();
                }
            }
        }
    );
    // Clicking any of the dynamically-generated X buttons on the list
    $(document).on('click', ".removeSeat", function () {
            // Getting the Id of the Seat

            var id = $(this).attr('id').split(":");

            var t = parseInt(seats.indexOf(id[1]));
            if (t == -1) {

            } else {

                seats.splice(t, 1);
            }

            var price = $(this).attr('value')
            $('#seatsList .' + id[1]).remove();
            $("#" + id[1] + ".seatNumber").removeClass("seatSelected");
            removeFromCheckout(price);
            refreshCounter();
        }
    );
    // Show tooltip on hover.
    $(".seatNumber").hover(
        function () {
            if (!$(this).hasClass("seatUnavailable")) {
                var id = $(this).attr('id');
                var id = id.split("_");
                var price = $(this).attr('value');
                var tooltip = "Rząd: " + id[0] + " Miejsce:" + id[1] + " Cena:PLN" + price;

                $(this).prop('title', tooltip);
            } else {
                $(this).prop('title', "Miejsce zarezerwowane");
            }
        }
    );

    // Function to refresh seats counter
    function refreshCounter() {
        $(".seatsAmount").text($(".seatSelected").length);
    }

    // Add seat to checkout
    function addToCheckout(thisSeat) {
        var seatPrice = parseInt(thisSeat);
        var num = parseInt($('.txtSubTotal').text());
        num += seatPrice;
        num = num.toString();
        $('.txtSubTotal').text(num);
        $('.sw-btn-next').attr('disabled', false);
    }

    // Remove seat from checkout
    function removeFromCheckout(thisSeat) {
        var seatPrice = parseInt(thisSeat);
        var num = parseInt($('.txtSubTotal').text());
        num -= seatPrice;
        var selectedSeats = parseInt($(".seatSelected").length);
        if (selectedSeats == 0) {
            $('.sw-btn-next').attr('disabled', true);
        }
        num = num.toString();
        $('.txtSubTotal').text(num);
    }

    // Clear seats.
    $("#btnClear").click(
        function () {
            $('.txtSubTotal').text(0);
            $(".seatsAmount").text(0);
            $('.seatSelected').removeClass('seatSelected');
            $('#seatsList li').remove();
            $('.sw-btn-next').attr('disabled', true);
        }
    );
});









































